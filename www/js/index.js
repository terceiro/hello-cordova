var accelerometer = null

function setup_accelerometer() {
    var accelerometer_options = {
        frequency: 250
    }
    accelerometer = navigator.accelerometer.watchAcceleration(
            device_moved, device_motion_failed, accelerometer_options)
}

var position_template = '{x}, {y}, {z}'

function device_moved(acceleration) {
    var element = document.getElementById('position')
    var position = position_template
        .replace('{x}', acceleration.x)
        .replace('{y}', acceleration.y)
        .replace('{z}', acceleration.z)
    element.innerHTML = position
}

function device_motion_failed() {
    console.log("Failed to capture device movement")
}

document.addEventListener('deviceready', setup_accelerometer, false)
