apk = platforms/android/build/outputs/apk/android-debug.apk
src = config.xml Makefile $(shell find www/ hooks/ -type f)

all: $(apk)

$(apk): .android.stamp $(src)
	cordova build
	touch $(apk)

.android.stamp:
	cordova platform add android
	touch $@

adb-install: $(apk)
	adb install -r $(apk)

clean:
	rm -rf platforms/ plugins/ .android.stamp
